#!/usr/bin/python3

from ab23.msg import MotorPowerData

from ab23_states import Init, Evaluate, SearchForGate, SteerToGate, GetWaypoint, SteerToWaypoint, Dead
from ab23_types import ControllerState, AbState

from typing import Tuple


class ControllerStateMachine:
    
    def __init__(self):
        self.Init = Init()
        self.SearchForGate = SearchForGate()
        self.SteerToGate = SteerToGate()
        self.Evaluate = Evaluate()
        self.GetWaypoint = GetWaypoint()
        self.SteerToWaypoint = SteerToWaypoint()
        self.Dead = Dead()

    def __call__(self, state: AbState) -> Tuple[MotorPowerData, ControllerState]:
        if state.GetState() == ControllerState.INIT:
            return self.Init(state)
        elif state.GetState() == ControllerState.SEARCH_FOR_GATE:
            return self.SearchForGate(state)
        elif state.GetState() == ControllerState.STEER_TO_GATE:
            return self.SteerToGate(state)
        elif state.GetState() == ControllerState.EVALUATE:
            return self.Evaluate(state)
        elif state.GetState() == ControllerState.GET_WAYPOINT:
            return self.GetWaypoint(state)
        elif state.GetState() == ControllerState.STEER_TO_WAYPOINT:
            return self.SteerToWaypoint(state)
        elif state.GetState() == ControllerState.DEAD:
            return self.Dead(state)
        else:
            raise RuntimeError("state_machine: no such state possible")
