#!/usr/bin/python3

def core_test_ctrl_cmd_id_mapper(cmd_id: int) -> str:
    if cmd_id == 1:
        return "CONTROL_ON"
    elif cmd_id == 2:
        return "CONTROL_OFF"
    elif cmd_id == 5:
        return "CLEAN_MSG_QUEUE"
    else:
        return "NOOP"

def detection_str_to_color_mapper(id: int) -> str:
    if id == 0:
        return "BLACK"
    elif id == 2 or id == 3:
        return "GREEN"
    elif id == 4 or id == 5:
        return "RED"
    elif id == 6:
        return "YELLOW"
    elif id == 1:
        return "BLUE"
    else:
        raise Exception("No such ID")

if __name__ == "__main__":
    raise RuntimeError("This isn't an executable script. Use only as an import.")
