#!/usr/bin/python3

import numpy as np

from generators.nn_data_generators import gen_red_green

import random

if __name__ == "__main__":
    random.seed(None)
    gen_list_100 = [gen_red_green() for _ in range(0, 100)]
    gen_list_1000 = [gen_red_green() for _ in range(0, 1000)]

    # 100:
    print(f"for 100 generated data:\n==========================")
    print(f"RED bbox_x0:        average={np.mean([elem[0].bbox_x0 for elem in gen_list_100]):.6f}           std_deviation={np.std([elem[0].bbox_x0 for elem in gen_list_100]):.6f}")
    print(f"RED bbox_x1:        average={np.mean([elem[0].bbox_x1 for elem in gen_list_100]):.6f}           std_deviation={np.std([elem[0].bbox_x1 for elem in gen_list_100]):.6f}")
    print(f"RED bbox_y1:        average={np.mean([elem[0].bbox_y0 for elem in gen_list_100]):.6f}           std_deviation={np.std([elem[0].bbox_y0 for elem in gen_list_100]):.6f}")
    print(f"RED bbox_y1:        average={np.mean([elem[0].bbox_y1 for elem in gen_list_100]):.6f}           std_deviation={np.std([elem[0].bbox_y1 for elem in gen_list_100]):.6f}")
    print(f"RED heading_diff:   average={np.mean([elem[0].heading_diff for elem in gen_list_100]):.6f}           std_deviation={np.std([elem[0].heading_diff for elem in gen_list_100]):.6f}")
    print(f"RED distance:       average={np.mean([elem[0].distance for elem in gen_list_100]):.3f}           std_deviation={np.std([elem[0].distance for elem in gen_list_100]):.6f}")
    print(f"GREEN bbox_x0:      average={np.mean([elem[1].bbox_x0 for elem in gen_list_100]):.6f}           std_deviation={np.std([elem[1].bbox_x0 for elem in gen_list_100]):.6f}")
    print(f"GREEN bbox_x1:      average={np.mean([elem[1].bbox_x1 for elem in gen_list_100]):.6f}           std_deviation={np.std([elem[1].bbox_x1 for elem in gen_list_100]):.6f}")
    print(f"GREEN bbox_y0:      average={np.mean([elem[1].bbox_y0 for elem in gen_list_100]):.6f}           std_deviation={np.std([elem[1].bbox_y0 for elem in gen_list_100]):.6f}") 
    print(f"GREEN bbox_y1:      average={np.mean([elem[1].bbox_y1 for elem in gen_list_100]):.6f}           std_deviation={np.std([elem[1].bbox_y1 for elem in gen_list_100]):.6f}")
    print(f"GREEN heading_diff: average={np.mean([elem[1].heading_diff for elem in gen_list_100]):.6f}           std_deviation={np.std([elem[1].heading_diff for elem in gen_list_100]):.6f}")
    print(f"GREEN distance:     average={np.mean([elem[1].distance for elem in gen_list_100]):.3f}           std_deviation={np.std([elem[1].distance for elem in gen_list_100]):.6f}")
    # 1000:
    print(f"\nfor 1000 generated data:\n==========================")
    print(f"RED bbox_x0:        average={np.mean([elem[0].bbox_x0 for elem in gen_list_1000]):.6f}           std_deviation={np.std([elem[0].bbox_x0 for elem in gen_list_1000]):.6f}")
    print(f"RED bbox_x1:        average={np.mean([elem[0].bbox_x1 for elem in gen_list_1000]):.6f}           std_deviation={np.std([elem[0].bbox_x1 for elem in gen_list_1000]):.6f}")
    print(f"RED bbox_y1:        average={np.mean([elem[0].bbox_y0 for elem in gen_list_1000]):.6f}           std_deviation={np.std([elem[0].bbox_y0 for elem in gen_list_1000]):.6f}")
    print(f"RED bbox_y1:        average={np.mean([elem[0].bbox_y1 for elem in gen_list_1000]):.6f}           std_deviation={np.std([elem[0].bbox_y1 for elem in gen_list_1000]):.6f}")
    print(f"RED heading_diff:   average={np.mean([elem[0].heading_diff for elem in gen_list_1000]):.6f}           std_deviation={np.std([elem[0].heading_diff for elem in gen_list_1000]):.6f}")
    print(f"RED distance:       average={np.mean([elem[0].distance for elem in gen_list_1000]):.3f}           std_deviation={np.std([elem[0].distance for elem in gen_list_1000]):.6f}")
    print(f"GREEN bbox_x0:      average={np.mean([elem[1].bbox_x0 for elem in gen_list_1000]):.6f}           std_deviation={np.std([elem[1].bbox_x0 for elem in gen_list_1000]):.6f}")
    print(f"GREEN bbox_x1:      average={np.mean([elem[1].bbox_x1 for elem in gen_list_1000]):.6f}           std_deviation={np.std([elem[1].bbox_x1 for elem in gen_list_1000]):.6f}")
    print(f"GREEN bbox_y0:      average={np.mean([elem[1].bbox_y0 for elem in gen_list_1000]):.6f}           std_deviation={np.std([elem[1].bbox_y0 for elem in gen_list_1000]):.6f}") 
    print(f"GREEN bbox_y1:      average={np.mean([elem[1].bbox_y1 for elem in gen_list_1000]):.6f}           std_deviation={np.std([elem[1].bbox_y1 for elem in gen_list_1000]):.6f}")
    print(f"GREEN heading_diff: average={np.mean([elem[1].heading_diff for elem in gen_list_1000]):.6f}           std_deviation={np.std([elem[1].heading_diff for elem in gen_list_1000]):.6f}")
    print(f"GREEN distance:     average={np.mean([elem[1].distance for elem in gen_list_1000]):.3f}           std_deviation={np.std([elem[1].distance for elem in gen_list_1000]):.6f}")
    