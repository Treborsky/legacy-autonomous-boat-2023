#!/usr/bin/python3

from ab23.msg import Zed2VideoData

import numpy as np

def gen_ndarray(shape) -> np.ndarray:
    return np.random.randint(
        low=0, 
        high=256, 
        size=shape, 
        dtype=np.uint8
    )

def gen_zed2_video_data(ref: int) -> Zed2VideoData:
    return Zed2VideoData(
        rgb=gen_ndarray((640, 640, 3)).ravel().tolist(),
        depth=gen_ndarray((640, 640, 3)).ravel().tolist(),
        zed2_video_data_ref=ref
    )
