#!/usr/bin/python3

from ab23.msg import GpsRtkData

import random

def gen_gps_rtk() -> GpsRtkData:
    msg = GpsRtkData()
    msg.latitude = int(random.normalvariate(50.06, 0.008) * 10000000) # d9 cords
    msg.longitude = int(random.normalvariate(19.91, 0.008) * 10000000)# d9 cords
    msg.speed = [int(random.normalvariate(1.5, 0.5)), int(random.normalvariate(1.5, 0.5)), int(random.normalvariate(1.5, 0.5))]
    msg.angle_track = int((random.randrange(314)-314) * 100)
    return msg

