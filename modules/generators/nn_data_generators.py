#!/usr/bin/python3

from ab23.msg import NnData

import random

from typing import Tuple


def gen_xy(x0, y0) -> Tuple[float, float]:
    x = 0.1
    y = random.normalvariate(0.1, 0.05)
    
    if x0 + x > 1.0:
        x = 1.0 - x0
    if y0 + y > 1.0:
        y = 1.0 - y0
    
    return (x, y)

def gen_red_x0y0() -> Tuple[float, float]:
    x0 = random.normalvariate(0.25, 0.2)
    y0 = random.normalvariate(0.6, 0.2)

    if x0 > 0.9:
        x0 = 0.9
    elif x0 < 0.0:
        x0 = 0.0

    if y0 > 0.7:
        y0 = 0.7
    elif y0 < 0.0:
        y0 = 0.0

    return (x0, y0)

def gen_red() -> NnData:
    x0, y0 = gen_red_x0y0()
    x, y = gen_xy(x0, y0)
    x1 = x + x0
    y1 = y + y0
    return NnData(
            bbox_x0 = x0,
            bbox_x1 = x1,
            bbox_y0 = y0,
            bbox_y1 = y1,
            heading_diff = 0.0, #abs(0.5 - ((x1 - x0) / 2.0 + x0)),
            distance = random.randrange(2000, 4000),
            detection_str = "RED"
        )

def gen_red_green() -> Tuple[NnData, NnData]:
    red = gen_red()
    gx0 = max(red.bbox_x1, 0.75 + random.normalvariate(0.25, 0.25))
    gx1 = min(1.0, gx0 + 0.1)
    green = NnData(
        bbox_x0 = gx0,
        bbox_x1 = gx1,
        bbox_y0 = 0.3,
        bbox_y1 = 0.6,
        heading_diff = 0.0, #abs(0.5 - ((gx1 - gx0) / 2.0 + gx0)),
        distance = random.randrange(160, 3500),
        detection_str = "GREEN"
    )
    return (red, green)

def gen_yellow(green: NnData, red: NnData) -> NnData:
    boundary_x0 = green.bbox_x1
    boundary_x1 = red.bbox_x0
    x0 = (random.random() * (boundary_x1-boundary_x0)) + boundary_x0
    x1 = (random.random() * (boundary_x1 - x0)) + x0
    black = NnData(
        bbox_x0 = x0,
        bbox_x1 = x1,
        bbox_y0 = 0.3,
        bbox_y1 = 0.6,
        heading_diff = abs(0.5 - ((x1 - x0) / 2.0 + x0)),
        distance =  random.randrange(2000, 4000),
        detection_str = "YELLOW"
    )
    return black

