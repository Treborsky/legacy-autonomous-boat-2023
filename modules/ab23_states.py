#!/usr/bin/python3

import rospy

from ab23.msg import MotorPowerData, NnData#, GpsRtkData, ImuData

from ab23_utils import measure_time, measure_time_only_throttle, ChangeState, ForwardStateNoCtrl
from ab23_mappers import detection_str_to_color_mapper
from ab23_types import AbState, ControllerState, Tasks
import ab23_algorithms as Logic


class Init:

    def __init__(self):
        pass

    def __call__(self, state: AbState):
        if Logic.SelectGateForSteeringFromDetections(state):
            return ChangeState(state.GetState(), ControllerState.STEER_TO_GATE, state.GetTask())
        else:
            return ChangeState(state.GetState(), ControllerState.EVALUATE, state.GetTask())

"""
    Choose the next state based on provided AbState which contains:
        - nn data list
        - latest gps info
        - waypoints

    State transitions:
        (EVALUATE -> INIT):
            prohibited
        (EVALUATE -> EVALUATE):
            if the evaluation time hasn't run out and 
            we're haven't been able to transition to any other state
        (EVALUATE -> GET_WAYPOINT):
            if we are runnning of these tasks 
            [NAV_CHANNEL, MAGELLANS_ROUTE,
             NORTHERN_PASSAGE, OCEAN_CLEANUP]
            and we're haven't been able to get any detections which 
            can keep us going for the last 10 seconds
        (EVALUATE -> STEER_TO_WAYPOINT):
            prohibited
        (EVALUATE -> SEARCH_FOR_GATE): 
            if we are running one of tasks 
            [NAV_CHANNEL, MAGELLANS_ROUTE,
             NORTHERN_PASSAGE]
            and we haven't received any detections we can steer on right now
        (EVALUATE -> STEER_TO_GATE):
            prohibited
        (EVALUATE -> DIE):
            either if the evaluation time has expired or the evaluate counter
            added up to 100 
"""
class Evaluate:

    def __init__(self):
        self.evaluate_counter = 0
    
    def __call__(self, state: AbState):
        self.evaluate_time_start = rospy.get_time()
        self.evaluate_time_expire_time = self.evaluate_time_start + 10.0
        self.evaluate_counter += 1
        if self.evaluate_counter:
            pass
        elif state.GetTask() in [Tasks.NAV_CHANNEL,
                                 Tasks.MAGELLANS_ROUTE,
                                 Tasks.NORTHERN_PASSAGE]:
            if Logic.SelectGateForSteeringFromDetections(state):
                # task handling here
                return ChangeState(state.GetState(), ControllerState.STEER_TO_GATE, state.GetTask())
            # elif Logic.CheckIfNewWayPointArrived(state):
                # task handling here
                # return ChangeState(state.GetState(), ControllerState.STEER_TO_WAYPOINT, state.GetTask())
            else:
                # task handling here
                return ForwardStateNoCtrl(state.GetState(), ControllerState.EVALUATE, state.GetTask())
        elif self.evaluate_counter >= state.evaluate_max or rospy.get_time() > self.evaluate_time_expire_time:
            return ChangeState(state.GetState(), ControllerState.DEAD, state.GetTask())
        else:
            self.evaluate_counter = 0
            raise RuntimeWarning("Going to INIT state, state not allwed")


class SearchForGate:

    def __init__(self):
        pass

    def __call__(state: AbState):
        return ChangeState(state.GetState(), ControllerState.DEAD, state.GetTask())        


class SteerToGate:

    def __init__(self):
        pass

    def __call__(self, state: AbState):
        gate = Logic.SelectGateForSteeringFromDetections(state)
        distance = Logic.GetDistanceToGateCenter(gate)
        angle = Logic.GetAngleToGateCenter(gate, Logic.DistanceToPower(distance))
        msg = Logic.DistanceAngleToMotorSetValues(distance, angle, [gate[0].nn_data_ref, gate[1].nn_data_ref])
        if msg.left == 0 and msg.right == 0:
            ForwardStateNoCtrl(state.GetState(), state.GetTask())
        return msg, state.GetState()


class GetWaypoint:

    def __init__(self):
        self.get_waypoint_retry_counter = 0

    def __call__(self, state: AbState):
        if self.get_waypoint_retry_counter < state.get_waypoint_max:
            if state.GetNewWaypoint():
                return MotorPowerData(), ControllerState.STEER_TO_WAYPOINT
            else:
                return MotorPowerData(), ControllerState.EVALUATE
        else:
            return MotorPowerData(), ControllerState.EVALUATE


class SteerToWaypoint:

    def __init__(self):
        pass

    def __call__(self, state: AbState):
        if state.current_waypoint is None:
            return MotorPowerData(), ControllerState.EVALUATE
        elif Logic.CheckIfWaypointAchieved(state):
            return Logic.WaypointAndBoatPosToMotorValues(state), ControllerState.STEER_TO_WAYPOINT
        



class Dead:

    def __init__(self):
        pass

    def __call__(state: AbState):
        [sub.unregister() for sub in state.GetSubscribers()]
        return MotorPowerData(), state.GetState()