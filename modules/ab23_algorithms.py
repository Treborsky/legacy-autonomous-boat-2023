#!/usr/bin/python3

import rospy

from ab23.msg import MotorPowerData, NnData#, ImuData, GpsRtkData

import ab23_utils
from ab23_utils import ChangeState, ForwardStateNoCtrl
from ab23_types import ControllerState, Tasks, AbState, Waypoint
from ab23_mappers import detection_str_to_color_mapper

from typing import List, Tuple

from numpy import arctan, sqrt, radians, sin, cos, arctan2, degrees

# boat's physical parameters
TURN_SCALE = .3
POWER_CONST = 500
TURN_CONST = 150

# zed2i camera parameters
MAX_DISTANCE = 900.0 # [cm]
MIN_DISTANCE = 20.0 # [cm]
MAX_VIEW = 36.0

# mathematical and physical constants
RAD_TO_DEG = 57.2957795


# SteerToWaypoint utilities
def WaypointAndBoatPosToMotorValues(state: AbState) -> MotorPowerData():
    wp_lat, wp_lon = state.current_waypoint.GetLatLon()
    boat_lat, boat_lon, boat_course = state.gps_rtk_data.latitude, state.gps_rtk_data.longitude, state.gps_rtk_data.course
    err_dist = CartesianDistanceBetweenTwoPoints(wp_lat, boat_lat, wp_lon, boat_lon)
    err_angle = CourseDifferenceBetweenBoatAndWaypoint(wp_lat, boat_lat, wp_lon, boat_lon, boat_course)
    return GpsDistanceAngleToMotorSetValues(err_dist, err_angle)

def GpsDistanceAngleToMotorSetValues(distance, angle, nn_data_refs: Tuple[int, int]) -> MotorPowerData:
    rospy.logdebug(f"DistanceAngleToMotorSetValues: [distance: {distance}, angle: [{angle}], refs: [{nn_data_refs}]]")
    msg = MotorPowerData()

    left, right = 0,0
    if distance < 8e-6:
        left, right = POWER_CONST / 1.5
    else:
        left, right = POWER_CONST, POWER_CONST

    if angle == 0:
        left += TURN_CONST
        right += TURN_CONST
    elif angle > 0:
        left -= TURN_CONST
        right += TURN_CONST
    elif angle < 0:
        left += TURN_CONST
        right -= TURN_CONST

    # convert to values accepted in embedded system
    msg.left = NormalizedToEmbeddedSystemAcceptable(left)
    msg.right = NormalizedToEmbeddedSystemAcceptable(right)
    
    if len(nn_data_refs) > 1:
        msg.nn_data_green_ref = nn_data_refs[1]
        msg.nn_data_red_ref = nn_data_refs[0]

    return msg

def CourseDifferenceBetweenBoatAndWaypoint(wp_lat, boat_lat, wp_lon, boat_lon, course) -> float:
    lat1 = radians(boat_lat)
    lat2 = radians(wp_lat)
    delta_long = radians(boat_lon - wp_lon)
    y = sin(delta_long)*cos(lat2)
    x = cos(lat1)*sin(lat2) - sin(lat1)*cos(lat2)*cos(delta_long)
    course_correct = degrees(arctan2(y, x))
    return course - course_correct

def CheckIfWaypointAchieved(state: AbState) -> bool:
    wp_lat, wp_lon = state.current_waypoint.GetLatLon()
    boat_lat, boat_lon = state.gps_rtk_data.latitude, state.gps_rtk_data.longitude
    if CartesianDistanceBetweenTwoPoints(wp_lat, boat_lat, wp_lon, boat_lon) <= state.waypoint_nav_threshold:
        return True
    else:
        return False

def CartesianDistanceBetweenTwoPoints(x0, x1, y0, y1) -> float:
    return sqrt((x1 - x0) ** 2 + (y1 - y0) ** 2)

# SteerToGate utilities
def SelectGateForSteeringFromDetections(state: AbState) -> Tuple[NnData, NnData]:
    detections = FilterSmallDetections(state.GetDetections())
    left = NnData(distance=9999.0)
    right = NnData(distance=9999.0)
    for det in detections:
        if det.detection_str == "GREEN":
            if det.distance > 160.0 and det.distance < 3500.0 and right.distance > det.distance:
                right = det
        elif det.detection_str == "RED":
            if det.distance > 160.0 and det.distance < 3500.0 and left.distance > det.distance:
                left = det
    return left, right
    
def FilterSmallDetections(detections: List[NnData]) -> List[NnData]:
    return [det for det in detections if Area(det) > 0.0]

def Area(det: NnData) -> float:
    return (det.bbox_x1 - det.bbox_x0) * (det.bbox_y1 - det.bbox_y0)

def GetDistanceToGateCenter(gate) -> float:
    return (gate[0].distance + gate[1].distance) / 2.0

# calculates the center of a gate in relatino to the middle of the screen
def GetAngleToGateCenter(gate, dist) -> float:
    left, right = gate[0], gate[1]
    center_left = (left.bbox_x1 + left.bbox_x0) / 2.0
    center_right = (right.bbox_x1 + right.bbox_x0) / 2.0
    center = (center_right + center_left) / 2.0
    angle = arctan(center/dist) * RAD_TO_DEG
    rospy.logdebug(f"GetAngleToGateCenter: [center: {center}, angle: {angle}]")
    return angle

# distance - non negative, we're not supporting steering backwards at this moment (power)
# angle - negative means we need to rotate counter clockwise, positive is clockwise (turn)
# refs - tuple contains [red, green, yellow, black]
def DistanceAngleToMotorSetValues(distance, angle, nn_data_refs: Tuple[int, int]) -> MotorPowerData:
    rospy.logdebug(f"DistanceAngleToMotorSetValues: [distance: {distance}, angle: [{angle}], refs: [{nn_data_refs}]]")
    msg = MotorPowerData()

    if angle >= 36 or angle <= -36 or distance < 160.0 or distance > 3500.0:
        return msg

    power = DistanceToPower(distance)
    turn = AngleToTurn(angle)

    left = (1 - TURN_SCALE) * power - TURN_SCALE * turn
    right = (1 - TURN_SCALE) * power + TURN_SCALE * turn

    # convert to values accepted in embedded system
    msg.left = int(NormalizedToEmbeddedSystemAcceptable(left))
    msg.right = int(NormalizedToEmbeddedSystemAcceptable(right))
    
    if len(nn_data_refs) > 1:
        msg.nn_data_green_ref = nn_data_refs[1]
        msg.nn_data_red_ref = nn_data_refs[0]

    return msg

# raw distance to normalized "power"
def DistanceToPower(distance):
    power = (distance - MIN_DISTANCE) / (MAX_DISTANCE - MIN_DISTANCE)
    return power

# converts raw angle diff to normalized "turn"
def AngleToTurn(angle):
    turn = angle / MAX_VIEW
    return turn 

# value - is from range []
# returned value - in range [-1000, 1000]
def NormalizedToEmbeddedSystemAcceptable(value):
    value = value * 1000
    return int(value)


if __name__ == "__main__":
    raise RuntimeError("This isn't an executable script. Use only as an import.")
