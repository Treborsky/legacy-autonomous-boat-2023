#!/usr/bin/python3

import rospy

from ab23.msg import NnData, GpsRtkData#, ImuData, MotorPowerData
from ab23.srv import WaypointService, WaypointServiceRequest, WaypointServiceResponse

from typing import List, Tuple
from enum import Enum

from numpy import sqrt, square


class ControllerState(Enum):
    INIT = "INIT",
    EVALUATE = "EVALUATE",
    GET_WAYPOINT = "GET_WAYPOINT",
    STEER_TO_WAYPOINT = "STEER_TO_WAYPOINT",
    SEARCH_FOR_GATE = "SEARCH_FOR_GATE",
    STEER_TO_GATE = "STEER_TO_GATE",
    DEAD = "DEAD"

class Tasks(Enum):
    NO_TASK = "NO_TASK"
    NAV_CHANNEL = "NAV_CHANNEL",
    MAGELLANS_ROUTE = "MAGELLANS_ROUTE",
    DOCKING_TASK = "DOCKING_TASK",
    NORTHERN_PASSAGE = "NORTHERN_PASSAGE",
    OCEAN_CLEANUP = "OCEAN_CLEANUP",
    FEED_THE_FISH = "FEED_THE_FISH"


class WaypointServiceType(str, Enum):
    GET_NEW_WAYPOINT = "GET_NEW_WAYPOINT",
    WAYPOINT_ACHIEVED = "WAYPOINT_ACHIEVED",
    ADD_NEW_WAYPOINT = "ADD_NEW_WAYPOINT",
    SAVE_WAYPOINTS = "SAVE_WAYPOINTS",
    NO_MORE_ENDPOINTS = "NO_MORE_ENDPOINTS"


class Waypoint:

    def __init__(self, name="unnamed_wp", lat=0.0, lon=0.0):
        self.name = name
        self.lat = lat
        self.lon = lon

    def __eq__(self, other) -> bool:
        return self.name == other.name and self.lat == other.lat and self.lon == other.lon

    def GetName(self) -> str:
        return self.name

    def GetLatLon(self) -> Tuple[float, float]:
        return self.lat, self.lon

    def GetDistanceToWaypoint(self, gps_rtk_data: GpsRtkData) -> float:
        return sqrt(square(self.lat - gps_rtk_data.lat) + square(self.lon - gps_rtk_data.lon))


class AbState:

    def __init__(self):
        self.evaluate_max = 100
        self.get_waypoint_max = 100
        self.nn_data_list: List[NnData] = []
        self.nn_data_list_len: int = 10
        self.nn_data_last_update = 0
        self.gps_rtk_data: GpsRtkData = GpsRtkData()
        self.current_waypoint: Waypoint = None
        self.waypoint_client = rospy.ServiceProxy("/core/waypoint_service", WaypointService)
        self.sub_nn_data = rospy.Subscriber("/nn/data", NnData, self.receive_nn_data)
        self.sub_gps_rtk_data = rospy.Subscriber("/gps_rtk/data", GpsRtkData, self.receive_gps_rtk_data)
        rospy.loginfo(f"STATE_CHANGE: [state_machine: {ControllerState.INIT}] [task: {Tasks.NO_TASK}]")
        self.task = Tasks.NO_TASK
        self.state = ControllerState.INIT

    def GetState(self):
        return self.state
    
    def GetTask(self):
        return self.task

    def GetDetections(self):
        return self.nn_data_list

    def GetGpsData(self) -> None:
        return None
    
    def GetNewWaypoint(self) -> bool:
        request = WaypointServiceRequest(
            request_type_req=WaypointServiceType.GET_NEW_WAYPOINT.value,
            waypoint_name_req="",
            latitude=0.0,
            longitude=0.0
        )
        try:
            response = self.waypoint_client.call(request)
            if response.request_type_res == WaypointServiceType.GET_NEW_WAYPOINT.value:
                self.current_waypoint = Waypoint(name=response.waypoint_name_res, lat=response.latitude, lon=response.longitude)
                return True
            else:
                return False
        except rospy.ServiceException:
            rospy.logwarn(f"service: {self.waypoint_client.uri} unavailable")
            raise RuntimeError("unable to get new waypoint")
        except rospy.ROSInterruptException:
            rospy.logwarn(f"service request cancelled by user")
            raise RuntimeError("unable to get new waypoint")
        except rospy.ROSSerializationException:
            rospy.logwarn(f"failed to serialize message: {request}, check the fields")
            raise RuntimeError("unable to get new waypoint")

    def NotifyWaypointAchieved(self, waypoint: Waypoint) -> bool:
        request = WaypointServiceRequest(
            request_type_req=WaypointServiceType.WAYPOINT_ACHIEVED.value,
            waypoint_name_req=waypoint.GetName(),
            latitude=waypoint.lat,
            longitude=waypoint.lon
        )
        try:
            response = self.waypoint_client.call(request)
            if response.request_type_res == WaypointServiceType.WAYPOINT_ACHIEVED.value:
                self.current_waypoint = None
                return True
            else:
                return False
        except rospy.ServiceException:
            rospy.logwarn(f"service: {self.waypoint_client.uri} unavailable")
            raise RuntimeError("unable to notify of achieved waypoint")
        except rospy.ROSInterruptException:
            rospy.logwarn(f"service request cancelled by user")
            raise RuntimeError("unable to notify of achieved waypoint")
        except rospy.ROSSerializationException:
            rospy.logwarn(f"failed to serialize message: {request}, check the fields")
            raise RuntimeError("unable to notify of achieved waypoint")

    def GetSubscribers(self):
        return [self.sub_nn_data, self.sub_gps_rtk_data]

    def receive_nn_data(self, msg: NnData) -> None:
        if len(self.nn_data_list) == self.nn_data_list_len:
            self.nn_data_list.pop()
        self.nn_data_list.insert(0, msg)

    def receive_gps_rtk_data(self, msg: GpsRtkData):
        return

