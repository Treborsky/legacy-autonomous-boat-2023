#!/usr/bin/python3

import unittest

from ab23.msg import MotorPowerData

from ab23_algorithms import DistanceAngleToMotorSetValues

class TestDistanceAngleToMotorSetValues(unittest.TestCase):
    def test_steerLeftWhenDistanceInRange(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(2000.0, 20.0, [])
        self.assertGreater(msg.left, 0)
        self.assertGreater(msg.right, 0)
        self.assertLess(msg.left, msg.right)

    def test_steerRightWhenDistanceInRange(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(2000.0, -20.0, [])
        self.assertGreater(msg.left, 0)
        self.assertGreater(msg.right, 0)
        self.assertLess(msg.right, msg.left)

    def test_rotateCounterClockwiseAlmostInPlaceWhenCloseToGate(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(200.0, 20.0, [])
        self.assertLess(msg.left, 0)
        self.assertGreater(msg.right, 0)
        self.assertLess(msg.left, msg.right)
    
    def test_rotateClockwiseAlmostInPlaceWhenCloseToGate(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(200.0, -20.0, [])
        self.assertGreater(msg.left, 0)
        self.assertLess(msg.right, 0)
        self.assertLess(msg.right, msg.left)

    def test_steerLeftWhenDistanceMaxRange(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(3500.0, 5.0, [])
        self.assertGreater(msg.left, 0)
        self.assertGreater(msg.right, 0)
        self.assertLess(msg.left, msg.right)

    def test_steerRightWhenDistanceMaxRange(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(3500.0, -5.0, [])
        self.assertGreater(msg.left, 0)
        self.assertGreater(msg.right, 0)
        self.assertLess(msg.right, msg.left)

    def test_steerLeftWhenDistanceMinRange(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(160.0, 5.0, [])
        self.assertLess(msg.left, 0)
        self.assertGreater(msg.right, 0)
        self.assertLess(msg.left, msg.right)

    def test_steerRightWhenDistanceMinRange(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(160.0, -5.0, [])
        self.assertGreater(msg.left, 0)
        self.assertLess(msg.right, 0)
        self.assertLess(msg.right, msg.left)

    def test_steerStraightWhenDistanceInRange(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(2000.0, 0.0, [])
        self.assertGreater(msg.left, 0)
        self.assertGreater(msg.right, 0)
        self.assertEqual(msg.right, msg.left)

    def test_steerStraightWhenDistanceMaxRange(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(3500.0, 0.0, [])
        self.assertGreater(msg.left, 0)
        self.assertGreater(msg.right, 0)
        self.assertEqual(msg.right, msg.left)

    def test_dontSteerWhenDistanceMinRange(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(160.0, 0.0, [])
        self.assertEqual(msg.left, 0)
        self.assertEqual(msg.right, 0)
        self.assertEqual(msg.right, msg.left)

    def test_steerLeftWhenAngleMaxRange(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(2000.0, 35.999, [])
        self.assertGreater(msg.left, 0)
        self.assertGreater(msg.right, 0)
        self.assertLess(msg.left, msg.right)
    
    def test_steerLeftWhenAngleMinRange(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(2000.0, -35.999, [])
        self.assertGreater(msg.left, 0)
        self.assertGreater(msg.right, 0)
        self.assertLess(msg.right, msg.left)

    def test_dontSteerWhenAngleOutOfRange(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(2000.0, 36, [])
        self.assertEqual(msg.left, 0)
        self.assertEqual(msg.right, 0)
        self.assertEqual(msg.left, msg.right)
    
    def test_dontSteerWhenDistanceOutOfRange(self):
        msg: MotorPowerData = DistanceAngleToMotorSetValues(150.0, 15.0, [])
        self.assertEqual(msg.left, 0)
        self.assertEqual(msg.right, 0)
        self.assertEqual(msg.left, msg.right)


if __name__ == "__main__":
    unittest.main()

