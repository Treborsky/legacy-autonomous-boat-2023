#!/usr/bin/python3

import torch
import numpy as np
import cv2


class NnModel:

    def __init__(self, model_best_path:str):
        self.threshold = 0.8
        self.yolov5_repo_path: str = "/home/solar/yolov5"
        self.model = self.load_model(self.yolov5_repo_path, model_best_path)
        self.classes = self.model.names
        torch.set_float32_matmul_precision('highest')
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'

    def load_model(self, repo_path: str, yolov5_path: str):
        model = torch.hub.load(repo_path, 'custom', path=yolov5_path, source='local')
        return model

    def object_detection(self, image: np.ndarray) -> dict:
        self.model.to(self.device)
        filtered_detections= {}
        selected_labels = []
        selected_cords = []
        results = self.model(image)
        detections = results.xyxyn[0][:, -1], results.xyxyn[0][:, :-1]
        labels = detections[0].cpu().numpy()
        cords_thresh = detections[1].cpu().numpy()
        for i in range(len(labels)):
            row = cords_thresh[i]
            if row[4] >= self.threshold:
                selected_labels.append(labels[i])
                selected_cords.append(cords_thresh[i])
        for object_id in selected_labels:
            for value in selected_cords:
                filtered_detections[int(object_id)] = value
                selected_cords.remove(value)
                break
        return filtered_detections

    def labels_to_name(self, labels):
        return self.classes[int(labels)]

    def plot_boxes(self, detections: dict, image: np.ndarray):
        labels = list(detections.keys())
        cords = list(detections.values())
        x_shape, y_shape = image.shape[1], image.shape[0]
        for i in range(len(labels)):
            row = cords[i]
            if row[4] >= self.threshold:
                x1, y1, x2, y2 = int(row[0]*x_shape), int(row[1]*y_shape), int(row[2]*x_shape), int(row[3]*y_shape)
                bgr = (0, 255, 0)
                cv2.rectangle(image, (x1, y1), (x2, y2), bgr, 2)
                cv2.putText(image, self.labels_to_name(labels[i]), (x1, y1), cv2.FONT_HERSHEY_SIMPLEX, 0.9, bgr, 2)
        return image

