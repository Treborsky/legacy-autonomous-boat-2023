#!/usr/bin/python3

import rospy

from ab23.msg import MotorPowerData
from ab23.srv import WaypointServiceResponse, WaypointServiceRequest

from ab23_types import ControllerState, Tasks, Waypoint

from functools import wraps
from typing import Tuple
import time

def measure_time_args(func):
    @wraps(func)
    def measure_wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        rospy.logdebug(f"function_name=[{func.__name__}] args=[{args}] kwargs=[{kwargs}] execution_time=[{total_time:.6f}s]")
        return result
    return measure_wrapper

def measure_time_throttle(func):
    @wraps(func)
    def measure_wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        rospy.logdebug_throttle(5, f"function_name=[{func.__name__}] args=[{args}] kwargs=[{kwargs}] execution_time=[{total_time:.6f}s]")
        return result
    return measure_wrapper

def measure_time_only_throttle(func):
    @wraps(func)
    def measure_wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        rospy.logdebug_throttle(5, f"function_name=[{func.__name__}] execution_time=[{total_time:.6f}s]")
        return result
    return measure_wrapper

def measure_time(func):
    @wraps(func)
    def measure_wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        rospy.logdebug(f"function_name=[{func.__name__}] execution_time=[{total_time:.6f}s]")
        return result
    return measure_wrapper 

def ChangeState(old_state, new_state, task=Tasks.NO_TASK, motor_control=MotorPowerData()) -> Tuple[MotorPowerData, ControllerState]:
    rospy.loginfo(f"STATE_CHANGE: [state_machine: {old_state} -> {new_state}] [task: {task}]")
    return motor_control, new_state

def ForwardStateNoCtrl(state, task) -> Tuple[MotorPowerData, ControllerState]:
    rospy.loginfo(f"STATE_CHANGE: [state_machine: {state}] [task: {task}]")
    return MotorPowerData(), state

def WaypointToWaypointServiceResponse(waypoint: Waypoint) -> WaypointServiceResponse:
    lat, lon = waypoint.lat, waypoint.lon
    res = WaypointServiceResponse(
        waypoint_name_res=waypoint.GetName(),
        latitude=lat,
        longitude=lon
    )
    return res

def WaypointToWaypointServiceRequest(waypoint: Waypoint) -> WaypointServiceRequest:
    lat, lon = waypoint.lat, waypoint.lon
    res = WaypointServiceRequest(
        waypoint_name_req=waypoint.GetName(),
        latitude=lat,
        longitude=lon
    )
    return res

def WaypointServiceRequestToWaypoint(req: WaypointServiceRequest) -> Waypoint:
    return Waypoint(name=req.waypoint_name_req, lat=req.latitude, lon=req.longitude)
