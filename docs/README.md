# Autonomous Boat 2023 documentation

Contains all diagrams and other informative documents about the project. For missing info, check out the wiki on the gitlab repository.

## Currently all designs and docs on [wiki](https://gitlab.com/agh-solar-boat/ab2023/-/wikis/home)
