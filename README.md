# AGH Solar Boat - Autonomous Boat project

Code for our autonomous boat that will compete at RoboBoat 2023.

# READ BEFORE CONTRIBUTING

## Structure

- .gitlab - devops platform specific utilities
- action - ROS action files
- cfg - ROS dynamic reconfigure definitions
- config - ROS parameter config .yaml files
- docs - project documentation
- include - header files
- launch - ROS launch files
- msg - ROS message definition files
- scripts - Python source files
- src - C++ source files
- srv - ROS service definition files
- test - unit tests (utest) and node-level tests (ntest)
- tools - scripts that make your life easier
- env_ab2023.sh - useful aliases and etc.
- CMakeLists.txt - ROS generated CMake project config/makefile generation file
- package.xml - ROS package manifest

## Git stuff

### Remotes

- origin

### Branches

The **main** branch is for code ready for testing on water. For working versions, we will use tags with version number.

A branch's name must be clear as to what development is being done on it.

### Commit message

The message should (doesn't have to always) start with one of the prefixes below:

- [FIX] -> fixes to issues
- [MSG] -> for changes in .msg files
- [SRV] -> for changes in .srv files
- [BUILD] -> changes in build-related files (CMakeLists.txt)
- [IMPL] -> new implementation / fixes in implementation
- [DOC] -> documentation changes

#### General

Keep commit messages quite brief, but be specific and descriptive. Don't commit with a message like _quickfix_, at least add the name of the impacted class, function, whatever. If you feel like it's necessary, write more.
