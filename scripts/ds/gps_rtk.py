#!/usr/bin/python3

import rospy

from ab23.msg import GpsRtkData
from ab23.srv import CanTestCtrl, CanTestCtrlRequest, CanTestCtrlResponse
from ab23.srv import SaveWaypoint, SaveWaypointRequest, SaveWaypointResponse

from ab23_utils import measure_time
import can


#prefix can info
POS_ID = 0x134
VEL_ID = 0x137
HED_ID = 0x310
COURSE_ID = 0x312

filters = [
    {"can_id": POS_ID, "can_mask": 0x7FF, "extended": False},
    {"can_id": VEL_ID, "can_mask": 0x7FF, "extended": False},
    {"can_id": HED_ID, "can_mask": 0x7FF, "extended": False},
    {"can_id": COURSE_ID, "can_mask": 0x7FF, "extended": False},
]

bus = can.interface.Bus(interface='socketcan',channel='can0', can_filters=filters)


class GpsRtk:
    
    def __init__(self):
        rospy.loginfo(f"STATE_CHANGE: [INIT_SENSOR]")
        self.produce_msg = rospy.get_param("/production", True)
        self.tim_pub_gps_rtk: rospy.Timer = rospy.Timer(rospy.Duration(nsecs=1_00_000_000), self.send_data)
        self.pub_gps_rtk_data: rospy.Publisher = rospy.Publisher("/gps_rtk/data", GpsRtkData, queue_size=10)
        self.test_ctrl = rospy.Service("/can/test/ctrl", CanTestCtrl, self.handle_test_ctrl)
        self.waypoint_save = rospy.Service("/gps/waypoint", SaveWaypoint, self.handle_waypoint_save)
        rospy.loginfo(f"STATE_CHANGE: [RUNNING]")

    def handle_waypoint_save(slef, req:SaveWaypointRequest) -> SaveWaypointResponse:
        res = SaveWaypointResponse()
        pos_flag = False
        while(pos_flag == False):
            bus_msg = bus.recv()
            byte_array = bus_msg.data
            if(bus_msg.arbitration_id == POS_ID):
                latitude = int.from_bytes(byte_array[:4],'little',signed = True) * 1e-7
                longitude = int.from_bytes(byte_array[4:8],'little', signed = True) * 1e-7
                res.waypoint_res =str(latitude) + ' ' + str(longitude) + ' ' + req.waypoint_name_req
                pos_flag = True
        return res
            
        
    
    def handle_test_ctrl(self, req:CanTestCtrlRequest) -> CanTestCtrlResponse:
        res = CanTestCtrlResponse()
        if req.cmd_id_req == 1:
            self.produce_msg = True
            res.cmd_id_res = 1
            rospy.logdebug(f"SRV_HANDLER: [CAN_TEST_CTRL: [SEND: [cmd_id_req={req.cmd_id_req}]]]")
            rospy.loginfo(f"STATE_CHANGE: [CAN_SEND_START]")
        elif req.cmd_id_req == 2:
            self.produce_msg = False
            res.cmd_id_res = 2
            rospy.logdebug(f"SRV_HANDLER: [CAN_TEST_CTRL: [STOP: [cmd_id_req={req.cmd_id_req}]]]")
            rospy.logdebug(f"SRV_HANDLER: [[cmd_id_req={req.cmd_id_req}]]")
            rospy.loginfo(f"STATE_CHANGE: [CAN_SEND_STOP]")
        else:
            res.cmd_id_res = 0
            rospy.logdebug(f"SRV_HANDLER: [CAN_TEST_CTRL: [NOOP: [cmd_id_req={req.cmd_id_req}]]]")
            rospy.logdebug(f"SRV_HANDLER: [[cmd_id_req={req.cmd_id_req}]]")
            rospy.loginfo(f"STATE_CHANGE: [CAN_SEND_START]")
        return res
    
    @measure_time
    def collecting_data_from_can(self):
        msg = GpsRtkData()
        pos_flag = False
        vel_flag = False
        hed_flag = False
        course_flag = False
        while((pos_flag == False) or (vel_flag == False) or (hed_flag == False) or (course_flag == False)):  # colecting all data to fill msg
            bus_msg = bus.recv()
            byte_array = bus_msg.data
            if(bus_msg.arbitration_id == POS_ID):
                rospy.logdebug_throttle(1,bus_msg, logger_name = "can_log_pos")
                msg.latitude = int.from_bytes(byte_array[:4],'little', signed = True)
                msg.longitude = int.from_bytes(byte_array[4:8],'little', signed = True)
                pos_flag = True
            elif(bus_msg.arbitration_id == VEL_ID):
                rospy.logdebug_throttle(1,bus_msg, logger_name = "can_log_vel")
                msg.speed[0] = int.from_bytes(byte_array[:2],'little', signed = True)
                msg.speed[1] = int.from_bytes(byte_array[2:4],'little', signed = True)
                msg.speed[2] = int.from_bytes(byte_array[4:6],'little', signed = True)
                vel_flag = True
            elif(bus_msg.arbitration_id == HED_ID):
                rospy.logdebug_throttle(1,bus_msg, logger_name = "can_log_hed")
                msg.heading = int.from_bytes(byte_array[:2],'little')
                hed_flag = True
            elif(bus_msg.arbitration_id == COURSE_ID):
                rospy.logdebug_throttle(1,bus_msg, logger_name = "can_log_course")
                msg.course = int.from_bytes(byte_array[:2],'little')
                course_flag = True
        return msg
    
    def send_data(self,evt: rospy.timer.TimerEvent):
        if self.produce_msg:
            msg = self.collecting_data_from_can()
            self.pub_gps_rtk_data.publish(msg)
            rospy.logdebug(f"MSG_SENT: GpsRtkData: [latitude={msg.latitude * 1e-7}, longitude={msg.longitude*1e-7}, course = {msg.course * 1e-2}, heading = {msg.heading * 1e-2}")


if __name__ == "__main__":
    rospy.init_node("gps_rtk", log_level=rospy.DEBUG)
    gps_rtk = GpsRtk()
    rospy.spin()
    