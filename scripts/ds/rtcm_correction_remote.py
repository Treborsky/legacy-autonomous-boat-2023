#!/usr/bin/python3
import rospy

from ab23.msg import RTCM

import datetime
from http.client import HTTPConnection
import base64
from pyrtcm import RTCMReader

def BasicAuth(username, password):
    token = base64.b64encode(f"{username}:{password}".encode('utf-8')).decode("ascii")
    return f'Basic {token}'

def ConnectionToNtrip(ntc):
    connection = HTTPConnection(ntc.ntrip_server)
    connection.request('GET', '/'+ntc.ntrip_stream, headers = ntc.headers)
    return connection
    
def GetData(response,data):
    rtcm_msg = RTCM()
    l1 = response.read(1)
    l2 = response.read(1)
    pkt_len = ((ord(l1)&0x3)<<8)+ord(l2)
    pkt = response.read(pkt_len)
    parity = response.read(3)
    if len(pkt) != pkt_len:
        rospy.logdebug(f"[RTCM] package length error")
        return False
    rtcm_msg.header.seq += 1
    rtcm_msg.header.stamp = rospy.get_rostime()
    rtcm_msg.data = data + l1 + l2 + pkt + parity
    msg = RTCMReader.parse(rtcm_msg.data)
    rospy.logdebug(f"[RTCM] id:[{msg.identity}]")
    return rtcm_msg

class NtripClient():
    def __init__(self):
        self.ntrip_server = "40.121.5.206:31000"
        self.ntrip_user = "Ab23Ntrip"
        self.ntrip_password = "ab23roboboat123!!"
        self.ntrip_stream = "FLSN"
        self.nmea_gga = "$GPGGA,"
        self.stop = False
        self.headers = {
            'Ntrip-Version': 'Ntrip/2.0',
            'User-Agent': 'NTRIP ntrip_ros',
            'Connection': 'close',
            'Authorization': BasicAuth(self.ntrip_user, self.ntrip_password)
        }
        self.pub_rtcm_data: rospy.Publisher = rospy.Publisher("/rtcm/data", RTCM, queue_size=10)
        self.tim_rtcm: rospy.Timer = rospy.Timer(rospy.Duration(nsecs=1_00_000_000), self.run)
        
    
    def run(self,evt: rospy.timer.TimerEvent):
        connection = ConnectionToNtrip(self)
        response = connection.getresponse()
        if response.status != 200:
            rospy.logdebug("[RTCM]conncection to ntrip gone wrong")
        rtcm_msg = RTCM()
        while True:
            try :
                data = response.read(1)
            except ConnectionResetError:
                rospy.logdebug("[RTCM] ConnectionResetError")
                break
            except:
                rospy.logdebug("[RTCM] error")
                break
            if ord(data)!=211:
                continue
            rtcm_msg = GetData(response,data)
            if(rtcm_msg == False):
                continue
            self.pub_rtcm_data.publish(rtcm_msg)
        connection.close()
        
        
        
if __name__ == '__main__':
    rospy.init_node("rtcm_remote", log_level=rospy.DEBUG)
    rtcm_correction = NtripClient()
    rospy.spin()
