#!/usr/bin/python3

import rospy
from rospy.timer import TimerEvent

from ab23.msg import NnData, Zed2VideoData

from ab23.srv import NnSaveDataCommand, NnSaveDataCommandRequest, NnSaveDataCommandResponse

import numpy as np
import cv2

#import ab23_utils
from ab23_mappers import detection_str_to_color_mapper
from nn_model import NnModel

import time
from datetime import datetime
from os import environ


class VisionSystemPipeline:

    def __init__(self, nn):
        self._nn = nn
        self.saved_img_nr = 0
        self._msg_ref_counter = 0
        self.save_data = False
        self._nn_data_root: str = '/home/solar/Pictures/nn_data/'
        self._sub_zed2_video_data: rospy.Subscriber = rospy.Subscriber(
            "/zed2/video/data", Zed2VideoData, self.cb_zed2_video_data, queue_size=1)
        self._pub_nn_data: rospy.Publisher = rospy.Publisher(
            "/nn/data", NnData, queue_size=5)
        self._nn_save_data_srv = rospy.Service("/nn/save_cmd", NnSaveDataCommand, self.handle_nn_data_save_cmd)

    def get_prediction_file_name(self) -> str:
        return self._nn_data_root + f"prediction_{datetime.now()}.png"

    def handle_nn_data_save_cmd(self, req: NnSaveDataCommandRequest) -> NnSaveDataCommandResponse:
        cmd = req.requested_cmd
        if cmd == 1:
            self.save_data = True
            rospy.logdebug(f"SRV_HANDLER: [NN_IMG_SAVE: [START]]")
        elif cmd == 0:
            self.save_data = False
            rospy.logdebug(f"SRV_HANDLER: [NN_IMG_SAVE: [STOP]]")
        else:
            pass
        return NnSaveDataCommandResponse(executed_cmd=cmd)


    def detection_to_nn_data(self, det, zed2msg) -> NnData:
        self._msg_ref_counter += 1
        if det:
            type_id = detection_str_to_color_mapper(det[0])
            coords = det[1]
            new_msg = NnData(
                    bbox_x0=coords[0],
                    bbox_x1=coords[2],
                    bbox_y0=coords[1],
                    bbox_y1=coords[3],
                    heading_diff=0.0,
                    distance=0,
                    detection_str=type_id,
                    zed2_video_data_ref = zed2msg.zed2_video_data_ref,
                    nn_data_ref = self._msg_ref_counter
                )
            return new_msg
        return None

#    @ab23_utils.measure_time
    def cb_zed2_video_data(self, msg: Zed2VideoData) -> None:
        img = np.array(msg.rgb).reshape(640, 640, 3) # r, g, b (640, 640, 3) 
        rospy.loginfo(f"IMG_SHAPE: {img.shape}")
        depth = np.array(msg.depth, dtype=np.float32).reshape(640, 640) # depth data (640, 640)
        # cv2.imwrite(f"/home/solar/Pictures/img_{self.img_nr}.jpg", img)
        r, g, b = img[:,:,0], img[:,:,1], img[:,:,2]
        nn_detections = self._nn.object_detection(np.stack([r, g, b], axis=-1).astype(np.uint8))
        for detection in nn_detections.items():
            msg = self.detection_to_nn_data(detection, msg)
            if msg:
                midpoint_x_coord_int = int(640 * ((msg.bbox_x1 - msg.bbox_x0) / 2.0 + msg.bbox_x0))
                midpoint_y_coord_int = int(640 * ((msg.bbox_y1 - msg.bbox_y0) / 2.0 + msg.bbox_y0))
                msg.distance = depth[midpoint_x_coord_int, midpoint_y_coord_int]
                self._pub_nn_data.publish(msg)
                rospy.logdebug(f"MSG_SENT: [NnData: [bbox=[x0={msg.bbox_x0}, x1={msg.bbox_x1}, y0={msg.bbox_y0}, y1={msg.bbox_y1}], heading_diff={msg.heading_diff}, distance={msg.distance}, detection_str={msg.detection_str}, zed2_video_data_ref={msg.zed2_video_data_ref}, nn_data_ref={msg.nn_data_ref}]]")
        if self.save_data:
            self.saved_img_nr += 1
            frame = self._nn.plot_boxes(nn_detections, np.stack([r, g, b], axis=-1).astype(np.uint8))
            cv2.imwrite(self.get_prediction_file_name(), frame) 
            rospy.logdebug(f"NN_IMG_SAVED: {self.saved_img_nr}")


if __name__ == "__main__":
    rospy.init_node("nn", log_level=rospy.DEBUG)
    nn_load_start = time.time()
    
    nn = NnModel(environ["MODEL_BEST_PATH"])
    rospy.loginfo_once(f"NN model load time: {time.time() - nn_load_start}")
    
    vs = VisionSystemPipeline(nn)
    rospy.loginfo(f"Vision system pipeline init time: {time.time() - nn_load_start}")
    rospy.spin()


