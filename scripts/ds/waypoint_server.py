#!/usr/bin/python3

import rospy

from ab23.srv import WaypointService, WaypointServiceRequest, WaypointServiceResponse

from ab23_types import Waypoint, WaypointServiceType
from ab23_utils import WaypointToWaypointServiceResponse,  WaypointServiceRequestToWaypoint

from typing import List

class WaypointServer:

    def __init__(self, waypoint_filename):
        self.waypoints: List[Waypoint] = self.load_waypoints(waypoint_filename)
        self.achieved_waypoints: List[Waypoint] = []
        self.service = rospy.Service("/core/waypoint_service", WaypointService, self.handle_waypoint_service)

    def load_waypoints(self, filename) -> List[Waypoint]:
        rospy.loginfo(f"[LOADING WAYPOINTS FROM: {filename}")
        #file structure: latidude, longitude, ...
        waypoints = []
        for line in open(filename, "r"):
            latitude = float(line[:line.find(',')])
            longitude = float(line[line.find(',') + 2:line.find(',') + 10])
            way: Waypoint()
            way.lat = latitude
            way.long = longitude
            waypoints.append(Waypoint)
            rospy.logdebug(f"[waypoint read] {latitude} {longitude}")
        return [Waypoint()]

    def handle_waypoint_service(self, req: WaypointServiceRequest) -> WaypointServiceResponse:
        rospy.logdebug(f"req: type:{req.request_type_req} diff to ADD_NEW_WAYPOINT str: ")
        if str(req.request_type_req) == str(WaypointServiceType.GET_NEW_WAYPOINT.value):
            return self.handle_get_new_waypoint(req)
        elif str(req.request_type_req) == str(WaypointServiceType.WAYPOINT_ACHIEVED.value):
            return self.handle_waypoint_achieved(req)
        elif str(req.request_type_req) == str(WaypointServiceType.ADD_NEW_WAYPOINT.value):
            return self.handle_add_new_waypoint(req)
        elif str(req.request_type_req) == str(WaypointServiceType.SAVE_WAYPOINTS.value):
            return self.handle_save_waypoints(req)
        else:
            return self.handle_unimplemented_service(req)
        
    def handle_get_new_waypoint(self, req: WaypointServiceRequest) -> WaypointServiceResponse:
        if self.waypoints and len(self.waypoints) > len(self.achieved_waypoints):
            new_waypoint = self.waypoints[len(self.achieved_waypoints)]
            res = WaypointToWaypointServiceResponse(new_waypoint)
            res.request_type_res = req.request_type_req
            return res
        else:
            return WaypointServiceResponse(request_type_res="NO_MORE_ENDPOINTS")

    def handle_waypoint_achieved(self, req: WaypointServiceRequest) -> WaypointServiceResponse:
        achieved_waypoint = WaypointServiceRequestToWaypoint(req)
        if achieved_waypoint == self.waypoints[len(self.achieved_waypoints)]:
            self.achieved_waypoints.append(achieved_waypoint)
            res = WaypointToWaypointServiceResponse(achieved_waypoint)
            res.request_type_res = WaypointServiceType.WAYPOINT_ACHIEVED.value
            return res
        else:
            return WaypointServiceResponse(request_type_res="INCORRECT_WAYPOINT_SENT_AS_ACHIEVED")

    def handle_add_new_waypoint(self, req: WaypointServiceRequest) -> WaypointServiceResponse:
        waypoint_to_add = WaypointServiceRequestToWaypoint(req)
        if waypoint_to_add not in self.waypoints:
            self.waypoints.append(waypoint_to_add)
            res = WaypointToWaypointServiceResponse(waypoint_to_add)
            res.request_type_res = WaypointServiceType.ADD_NEW_WAYPOINT.value
            return res
        else:
            return WaypointServiceResponse(request_type_res="WAYPOINT_ALREADY_IN_LIST_OF_WAYPOINTS")

    def handle_save_waypoints(self, req: WaypointServiceRequest) -> WaypointServiceResponse:
        # TODO: get filename from waypoint_name_res (used as filename or filepath in this case) 
        # and save the self.waypoint list there in sepcified format
        file = open(WaypointServiceRequest.waypoint_name_res,'w')
        for waypoint in self.waypoints:
            file.write(str(waypoint.lat)+", "+str(waypoint.lon))
        file.close()
        return WaypointServiceResponse(request_type_res="ADDED")

    def handle_unimplemented_service(self, req: WaypointServiceRequest) -> WaypointServiceResponse:
        return WaypointServiceResponse(request_type_res="NOT_IMPLEMENTED")

if __name__ == "__main__":
    rospy.init_node("waypoint_server", log_level=rospy.DEBUG)
    waypoint_server = WaypointServer("/home/solar/nbg_waypoints.yaml")
    rospy.spin()