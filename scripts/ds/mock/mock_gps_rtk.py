#!/usr/bin/python3

import rospy

from ab23.msg import GpsRtkData

from generators.gps_rtk_data_generators import gen_gps_rtk

gps_rtk_ref_counter = 0

def send_gps_rtk_data(pub) -> None:
    global gps_rtk_ref_counter
    gps_rtk_ref_counter += 1
    msg = gen_gps_rtk()
    pub.publish(msg)
    
if __name__ == "__main__":
    rospy.init_node("mock_gps_rtk", log_level=rospy.DEBUG)
    pub = rospy.Publisher("/gps_rtk/data", GpsRtkData, queue_size=15)
    rate = rospy.Rate(50)
    while not rospy.is_shutdown():
        send_gps_rtk_data(pub)
        rate.sleep()
