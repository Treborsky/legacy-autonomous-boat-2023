#!/usr/bin/python3

import rospy

from ab23.msg import Zed2VideoData

from playback.streamer_factory import StreamerFactory


def publish_zed2_video_data(pub: rospy.Publisher, msg: Zed2VideoData) -> None:
    pub.publish(msg)

def get_and_send_video_data(evt) -> None:
    pass

if __name__ == "__main__":
    rospy.init_node("playback", log_level=rospy.INFO)
    source = None
    try:
        source = StreamerFactory.create(rospy.get_param())

    pub = rospy.Publisher("/zed2/video/data", Zed2VideoData)
    tim = rospy.Timer(rospy.Duration(nsecs=100), get_and_send_video_data)

