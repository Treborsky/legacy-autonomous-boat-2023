#!/usr/bin/python3

import rospy

from ab23.msg import Zed2VideoData

from generators.zed2_video_data_generators import gen_zed2_video_data

msg_ref_counter = 0

def send_zed2_video_data(pub: rospy.Publisher) -> None:
    global msg_ref_counter
    msg_ref_counter += 1
    pub.publish(gen_zed2_video_data(msg_ref_counter))

if __name__ == "__main__":
    rospy.init_node("mock_zed2", log_level=rospy.DEBUG)
    pub = rospy.Publisher("/zed2/video/data", Zed2VideoData, queue_size=1)
    rate = rospy.Rate(15)
    while not rospy.is_shutdown():
        send_zed2_video_data(pub)
        rate.sleep()

