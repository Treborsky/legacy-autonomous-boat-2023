#!/usr/bin/python3

import rospy

from ab23.msg import NnData, Zed2VideoData

from generators.nn_data_generators import gen_red_green

detection_ref_counter = 0

def cb_pub_nn_data(msg, pub) -> None:
    global detection_ref_counter
    detection_ref_counter += 1
    red, green = gen_red_green()
    red.zed2_video_data_ref = msg.zed2_video_data_ref
    red.nn_data_ref = detection_ref_counter
    detection_ref_counter += 1
    green.zed2_video_data_ref = msg.zed2_video_data_ref
    green.nn_data_ref = detection_ref_counter
    rospy.logdebug(f"MSG_SENT: [NnData: [bbox=[x0={red.bbox_x0}, x1={red.bbox_x1}, y0={red.bbox_y0}, y1={red.bbox_y1}], heading_diff={red.heading_diff}, distance={red.distance}, detection_str={red.detection_str}, zed2_video_data_ref={red.zed2_video_data_ref}, nn_data_ref={red.nn_data_ref}]]")
    rospy.logdebug(f"MSG_SENT: [NnData: [bbox=[x0={green.bbox_x0}, x1={green.bbox_x1}, y0={green.bbox_y0}, y1={green.bbox_y1}], heading_diff={green.heading_diff}, distance={green.distance}, detection_str={green.detection_str}, zed2_video_data_ref={green.zed2_video_data_ref}, nn_data_ref={green.nn_data_ref}]]")
    pub.publish(red)
    pub.publish(green)

if __name__ == "__main__":
    rospy.init_node("mock_nn", log_level=rospy.DEBUG)
    pub = rospy.Publisher("/nn/data", NnData, queue_size=5)
    sub = rospy.Subscriber("/zed2/video/data", Zed2VideoData, cb_pub_nn_data, pub)
    rospy.spin()

