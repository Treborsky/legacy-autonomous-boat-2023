#!/usr/bin/python3

import rospy

import serial

from ab23.msg import RTCM

class RtcmSerialInject():
    def __init__(self):
        self.sub_rtcm_data: rospy.Subscriber = rospy.Subscriber("/rtcm/data", RTCM, self.inject)
        self.ser = serial.Serial('/dev/ttyTHS0',baudrate=115200)
    def inject(self, msg: RTCM):
        self.ser.write(msg.data)
        rospy.logdebug("RTCM sent to ellipse")
        
        
        

if __name__ == "__main__":
    rospy.init_node("rtcm_inject", log_level=rospy.INFO)
    rtcm_inject = RtcmSerialInject()
    rospy.spin()