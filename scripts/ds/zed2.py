#!/usr/bin/python3

import rospy
from rospy.timer import TimerEvent

from ab23.msg import Zed2VideoData
from ab23.srv import Zed2SaveImages, Zed2SaveImagesRequest, Zed2SaveImagesResponse
from PIL import Image
prod = rospy.get_param("/production", True)
if prod:
    import pyzed.sl as sl

from datetime import datetime

import cv2
#import ab23_utils
import numpy as np

class Zed2Context:

    def __init__(self) -> None:
        global prod
        rospy.loginfo(f"STATE_CHANGE: [INIT_CONTEXT]")
        self._recording_file_root: str = "/home/solar/recording_files/"
        self._images_file_root: str = "/home/solar/saved_images/"
        self._camera_model: str = "ZED2"
        if prod:
            self.cam_recording_parameters = sl.RecordingParameters(
                self.get_recording_file_name(), 
                # sl.SVO_COMPRESSION_MODE.HD264
            )
            self.cam_runtime_parameters = sl.RuntimeParameters(
                sensing_mode=sl.SENSING_MODE.FILL
            )
  
    def get_img_file_name(self) -> str:
        return self._images_file_root + f"{self._camera_model}_{datetime.now()}.png"

    def get_recording_file_name(self) -> str:
        return self._recording_file_root + f"{self._camera_model}_{datetime.now()}.svo"


class Zed2Manager(Zed2Context):

#    @ab23_utils.measure_time
    def __init__(self, cam) -> None:
        global prod
        super().__init__()
        rospy.loginfo(f"STATE_CHANGE: [INIT_MANAGER]")
        self._camera = cam
        self.save_img = False
        self._msg_ref_counter = 0
        self._saved_img_counter = 0
        if prod:
            self._img_l: sl.Mat = sl.Mat()
            self._img_r: sl.Mat = sl.Mat()
            self._depth_map: sl.Mat = sl.Mat()
        self._publisher = rospy.Publisher("/zed2/video/data", Zed2VideoData, queue_size=10)
        self._tim_video_capture = rospy.Timer(rospy.Duration(0, 25e7), self.send_data) # ~4 [Hz] / 15 = 1 [Hz]
        self._tim_image_capture = rospy.Timer(rospy.Duration(0, 25e7), self.save_images)
        self._srv_image_capture = rospy.Service("/zed2/srv/save_cmd", Zed2SaveImages, self.handle_img_save_command)
        rospy.loginfo(f"STATE_CHANGE: [RUNNING]")

#    @ab23_utils.measure_time
    def handle_img_save_command(self, req: Zed2SaveImagesRequest) -> Zed2SaveImagesResponse:
        cmd = req.requested_cmd
        if cmd == 1:
            self.save_img = True
            rospy.logdebug(f"SRV_HANDLER: [ZED2_IMG_SAVE: [START]]")
        elif cmd == 0:
            self.save_img = False
            rospy.logdebug(f"SRV_HANDLER: [ZED2_IMG_SAVE: [STOP]]")
        else:
            pass
        return Zed2SaveImagesResponse(executed_cmd=cmd)

    def send_data(self, _: TimerEvent) -> None:
        if prod:
            if self._camera.grab(self.cam_runtime_parameters) == sl.ERROR_CODE.SUCCESS:
                self._camera.retrieve_image(self._img_l, sl.VIEW.LEFT) # sl.MEM.GPU
                self._camera.retrieve_image(self._img_r, sl.VIEW.RIGHT)
                self._camera.retrieve_measure(self._depth_map, sl.MEASURE.DEPTH)  
                 
                img_np = cv2.resize(self._img_l.get_data(), (640, 640), interpolation = cv2.INTER_AREA) 
                rgb_np = img_np[:, :, :3] # converting to (640,640,3)
                depth_np = cv2.resize(self._depth_map.get_data(), (640, 640), interpolation=cv2.INTER_AREA)
                flat_rgb = rgb_np.ravel().tolist()
                flat_depth = depth_np.ravel().tolist()
                self._msg_ref_counter += 1
                msg = Zed2VideoData(rgb=flat_rgb, depth=flat_depth, zed2_video_data_ref=self._msg_ref_counter)
                self._publisher.publish(msg)
                rospy.loginfo(f"central_distance: {depth_np[320,320]}")
            rospy.logdebug(f"MSG_SENT: [Zed2VideoData: [height={img_np.shape[0]}, width={img_np.shape[1]}, depth={img_np.shape[2]} data=[-.-], depth_data=[-.-], zed2_video_data_ref={msg.zed2_video_data_ref}]]")
        else:
            rospy.logdebug(f"MSG_SENT: [Zed2VideoData: [\"mock, testing\"]]")

    def save_images(self, _: TimerEvent) -> None:
        if prod:
            if self.save_img:
                self._saved_img_counter += 1
                cv2.imwrite(self.get_img_file_name(), self._img_l.get_data())          
                rospy.logdebug(f"MSG_COUNT: [ZED2_IMAGE_SAVE: #{self._saved_img_counter}]")
            else:
                rospy.logdebug(f"MSG_COUNT: [ZED2_IMAGE_SAVE: [\"mock, testing\"]]")


if __name__ == "__main__":
    rospy.init_node("zed2", log_level=rospy.INFO)
    rospy.loginfo(f"STATE_CHANGE: [INIT_NODE]")
    if prod:
        cam_init_params = sl.InitParameters(
            camera_fps=30,
            depth_mode = sl.DEPTH_MODE.ULTRA,
            depth_minimum_distance=20.0,
            depth_maximum_distance=900.0, 
            coordinate_units = sl.UNIT.CENTIMETER,
            open_timeout_sec=2.0
        )
        rospy.loginfo(f"STATE_CHANGE: [INIT_CAM]")
        cam = sl.Camera()
        if cam.open(cam_init_params) is sl.ERROR_CODE.SUCCESS:
            cam_manager = Zed2Manager(cam)
    else:
        cam_manager = Zed2Manager(None)
    rospy.spin()

