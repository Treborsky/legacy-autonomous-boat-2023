#!/usr/bin/python3

import rospy

from ab23.msg import ImuData

import can


#prefix can info
GYR_ID = 0x122
ACC_ID = 0x121

filters = [
    {"can_id": GYR_ID, "can_mask": 0x7FF, "extended": False},
    {"can_id": ACC_ID, "can_mask": 0x7FF, "extended": False}
]

bus = can.interface.Bus(interface='socketcan',channel='can0', can_filters=filters)


class Imu:
    
    def __init__(self):
        rospy.loginfo(f"STATE_CHANGE: [INIT_SENSOR]")
        self.msg_ref_counter = 0
        self.tim_pub_imu: rospy.Timer = rospy.Timer(rospy.Duration(nsecs=1_00_000_000), self.send_data)
        self.pub_imu_data: rospy.Publisher = rospy.Publisher("/imu/data", ImuData, queue_size=10)
        rospy.loginfo(f"STATE_CHANGE: [RUNNING]")
        
    def collecting_data_from_can(self):
        msg = ImuData()
        gyr_flag = False
        acc_flag = False
        while((gyr_flag == False) or (acc_flag == False)):  # colecting all data to fill msg
            bus_msg = bus.recv()
            byte_array = bus_msg.data
            if(bus_msg.arbitration_id == GYR_ID):
                rospy.logdebug_throttle(2,bus_msg, logger_name = "can_log_gyr")
                msg.ang_vel[0] = int.from_bytes(byte_array[:2],'little')
                msg.ang_vel[1] = int.from_bytes(byte_array[2:4],'little')
                msg.ang_vel[1] = int.from_bytes(byte_array[4:6],'little')
                gyr_flag = True
            elif(bus_msg.arbitration_id == ACC_ID):
                rospy.logdebug_throttle(2,bus_msg, logger_name = "can_log_acc")
                msg.lin_acc[0] = int.from_bytes(byte_array[:2],'little')
                msg.lin_acc[1] = int.from_bytes(byte_array[2:4],'little')
                msg.lin_acc[2] = int.from_bytes(byte_array[4:6],'little')
                acc_flag = True
        self.msg_ref_counter += 1
        msg.imu_data_ref = self.msg_ref_counter
        return msg
    
    def send_data(self,evt: rospy.timer.TimerEvent):
        msg = self.collecting_data_from_can()
        self.pub_imu_data.publish(msg)
        rospy.logdebug(f"MSG_SENT: ImuData: [ang_vel=[{msg.ang_vel[0]}, {msg.ang_vel[1]}, {msg.ang_vel[2]}], lin_acc=[{msg.lin_acc[0]}, {msg.lin_acc[1]}, {msg.lin_acc[2]}], imu_data_ref={msg.imu_data_ref}]")


if __name__ == "__main__":
    rospy.init_node("imu", log_level=rospy.DEBUG)
    imu = Imu()
    rospy.spin()
    