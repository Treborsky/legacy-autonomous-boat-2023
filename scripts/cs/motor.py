#!/usr/bin/python3

import rospy

from ab23.msg import MotorPowerData
from ab23.srv import MotorsDisableCommand, MotorsDisableCommandRequest, MotorsDisableCommandResponse

import ab23_utils

from struct import pack

import can

from struct import pack

class MotorsDriver:

    def __init__(self):
        rospy.loginfo("STATE_CHANGE: [INIT_DRIVER]")
        self.bus = can.Bus(channel='can0', interface='socketcan')
        self.send = True
        self.motors_disable_srv = rospy.Service("/motor/disable_command", MotorsDisableCommand, self.handle_motors_disable_command)
        self.msg_counter: int = 0
        self.motor_power: MotorPowerData = MotorPowerData()
        self.sub_motor_power = rospy.Subscriber("/core/motor_power/data", MotorPowerData, self.cb_motor_power)
        rospy.loginfo("STATE_CHANGE: [RUNNING]")
    
    def cb_motor_power(self, msg: MotorPowerData) -> None:
        self.msg_counter += 1
        self.motor_power = msg
        byte_msg = pack('<hh', msg.left, msg.right)
        if self.send:
            can_msg = can.Message(arbitration_id=0x1, data=byte_msg, is_extended_id=False)
            self.bus.send(can_msg)
            rospy.logdebug_throttle(10, f"MSG_SEND_COUNT: [{self.msg_counter}]")

    @ab23_utils.measure_time
    def handle_motors_disable_command(self, req: MotorsDisableCommandRequest) -> MotorsDisableCommandResponse:
        cmd = req.requested_cmd
        if cmd == 1:
            self.send = False
            rospy.loginfo(f"STATE_CHANGE: [MOTOR_STOP]")
            rospy.logdebug(f"SRV_HANDLER: [MOTOR_DISABLE_CMD: [STOP]]")
        elif cmd == 0:
            self.send = True
            rospy.loginfo(f"STATE_CHANGE: [MOTOR_START]")
            rospy.logdebug(f"SRV_HANDLER: [MOTOR_DISABLE_CMD: [START]]")
        else:
            pass
        return MotorsDisableCommandResponse(executed_cmd=cmd)


if __name__ == "__main__":
    rospy.init_node("motor", log_level=rospy.INFO)
    if rospy.get_param("production", True):
        motors = MotorsDriver()
    rospy.spin()

