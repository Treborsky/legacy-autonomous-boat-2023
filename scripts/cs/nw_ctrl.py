#!/usr/bin/python3

import rospy

from ab23.srv import MotorsDisableCommand, MotorsDisableCommandRequest, MotorsDisableCommandResponse

import ab23_utils

import socket


class NwCtrl:

    def __init__(self, period_s: float):
        self.is_connected = True
        self.period_s = period_s
        self.motors_disable_command_client = rospy.ServiceProxy("/motor/srv/disable_command", MotorsDisableCommand)
        self.tim_periodical_detection = rospy.Timer(rospy.Duration(nsecs=int(period_s * 1e9)), self.detect_link_break)

    def fping(self, host="8.8.8.8", port=53, timeout=3) -> bool:
        try:
            socket.setdefaulttimeout(timeout)
            socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
            return True
        except socket.error as ex:
            return False

    @ab23_utils.measure_time
    def detect_link_break(self, _: rospy.timer.TimerEvent) -> None:
        was_connected = self.is_connected
        res = None
        self.is_connected = self.fping()
        if not self.is_connected and was_connected:
            rospy.loginfo("STATE_CHANGE: [DISCONNECTED]")
            try:
                res = self.motors_disable_command_client(MotorsDisableCommandRequest(requested_cmd=1))
            except rospy.ServiceException as se:
                rospy.logdebug(f"SRV_EXCEPTION: [response={res}, error{se}]")
        elif not was_connected and self.is_connected:
            rospy.loginfo("STATE_CHANGE: [CONNECTED]")
            try:
                res = self.motors_disable_command_client(MotorsDisableCommandRequest(requested_cmd=0))
            except rospy.ServiceException as se:
                rospy.logdebug(f"SRV_EXCEPTION: [response={res}, error{se}]")
        elif self.is_connected:
            rospy.logdebug_throttle(5, "CONNECTED")
        elif not self.is_connected:
            rospy.logdebug_throttle(5, "CONNECTED")


if __name__ == "__main__":
    rospy.init_node("nw_ctrl", log_level=rospy.INFO)
    nw_ctrl = NwCtrl(1) # [s]
    rospy.spin()
    
