#!/usr/bin/python3

import rospy

from ab23.msg import NnData, MotorPowerData
from cs.core import AbController, NAV_CHANNEL_BASE_SPEED, NAV_CHANNEL_TURN_ADJUST, bbox_middle, middle_point

import pytest

buoys = [NnData(bbox_x0=0.2, bbox_x1=0.3),
         NnData(bbox_x0=0.7, bbox_x1=0.8),
         NnData(bbox_x0=0.05, bbox_x1=0.15),
         NnData(bbox_x0=0.2, bbox_x1=0.3),
         NnData(bbox_x0=0.6, bbox_x1=0.8),
         NnData(bbox_x0=0.75, bbox_x1=0.85)]

bbox_middle_data = [(buoys[0], 0.25),
                    (buoys[1], 0.75),
                    (buoys[2], 0.1),
                    (buoys[3], 0.25),
                    (buoys[4], 0.7),
                    (buoys[5], 0.8)]

middle_point_data = [((buoys[0], buoys[1]), 0.5),
                     ((buoys[2], buoys[3]), 0.175),
                     ((buoys[4], buoys[5]), 0.75)]

# red, green tuples
algo_data = [(middle_point_data[0][0], MotorPowerData(left=NAV_CHANNEL_BASE_SPEED, right=NAV_CHANNEL_BASE_SPEED)),
             (middle_point_data[1][0], MotorPowerData(left=NAV_CHANNEL_BASE_SPEED + NAV_CHANNEL_TURN_ADJUST, right=NAV_CHANNEL_BASE_SPEED - NAV_CHANNEL_TURN_ADJUST)),
             (middle_point_data[2][0], MotorPowerData(left=NAV_CHANNEL_BASE_SPEED - NAV_CHANNEL_TURN_ADJUST, right=NAV_CHANNEL_BASE_SPEED + NAV_CHANNEL_TURN_ADJUST)),
             ((NnData(bbox_x0=0.1, bbox_x1=0.2), None), MotorPowerData(left=NAV_CHANNEL_BASE_SPEED + NAV_CHANNEL_TURN_ADJUST, right=NAV_CHANNEL_BASE_SPEED - NAV_CHANNEL_TURN_ADJUST)),
             ((None, NnData(bbox_x0=0.8, bbox_x1=0.9)), MotorPowerData(left=NAV_CHANNEL_BASE_SPEED - NAV_CHANNEL_TURN_ADJUST, right=NAV_CHANNEL_BASE_SPEED + NAV_CHANNEL_TURN_ADJUST))]

@pytest.mark.parametrize("input, expected", bbox_middle_data)
def test_BboxMiddleTest(input, expected):
    assert bbox_middle(input) == expected

@pytest.mark.parametrize("input, expected", middle_point_data)
def test_BuoyMiddleTest(input, expected):
    assert middle_point(bbox_middle(input[0]), bbox_middle(input[1])) == expected

@pytest.mark.parametrize("input, expected", algo_data)
def test_NavigationChannelTest(input, expected):
    msg = AbController.NavigationChannel(None, input[0], input[1]) 
    assert (msg.left, msg.right) == (expected.left, expected.right)