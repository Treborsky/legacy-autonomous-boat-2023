#!/usr/bin/python3

import rospy

from ab23.msg import MotorPowerData, GpsRtkData, NnData

from enum import Enum

def task_id_to_string(task_id: int) -> str:
    if task_id == 1:
        return "NAVIGATION_CHANNEL"
    elif task_id == 2:
        return "MAGELLANS_ROUTE"
    elif task_id == 3:
        return "DOCKING_PLATFORM"
    elif task_id == 4:
        return "NORTHERN_PASSAGE"
    else:
        return "NO_TASK"
        
# States as constants for simplicity
NO_TASK = 0
NAVIGATION_CHANNEL = 1
MAGELLANS_ROUTE = 2
DOCKING_PLATFORM = 3
NORTHERN_PASSAGE = 4

# course orientation
RIGHT = True
LEFT = False

### base physical steering values

## navigation channel params
NAV_CHANNEL_BASE_SPEED = 500
NAV_CHANNEL_TURN_ADJUST = 150

## magellans route params
MAGELLANS_ROUTE_BASE_SPEED = 400
MAGELLANS_ROUTE_TURN_ADJUST = 200

## docking platform params


GATE_MISSING_OFFSET = 0.2

class StateMachine:

    def __init__(self):
        self.state = NO_TASK
    
    def ChangeState(self, new_state: int) -> None:
        """Update state variable available in GetState"""
        rospy.loginfo(f"STATE_CHANGE: [{task_id_to_string(self.state)}] -> [{task_id_to_string(new_state)}]")
        self.state = new_state
    
    def GetState(self) -> int:
        return self.state


def bbox_middle(nn_data: NnData) -> float:
    return (nn_data.bbox_x1 - nn_data.bbox_x0) / 2.0 + nn_data.bbox_x0

def middle_point(smaller: float, bigger: float) -> float:
    return (bigger - smaller) / 2.0 + smaller

def motor_power_data_with_refs(left, right, red=NnData(), green=NnData()) -> MotorPowerData:
    if not red:
        red = NnData()
    if not green:
        green = NnData()
    return MotorPowerData(left=left, right=right, nn_data_red_ref=red.nn_data_ref, nn_data_green_ref=green.nn_data_ref)

class AbController:

    def __init__(self):
        rospy.loginfo("INIT CONTROLLER")
        self._msg_ref_counter = 0
        self.state_machine = StateMachine()
        self.gps_rtk_data = GpsRtkData()
        self.nn_data = NnData()
        self.closest_red_buoy = NnData()
        self.closest_green_buoy = NnData()
        self.course_orientation: bool = RIGHT
        self.tim_pub_motor_power = rospy.Timer(rospy.Duration(nsecs=50_000_000), self.send_data)
        self.pub_motor_power = rospy.Publisher("/core/motor_power/data", MotorPowerData, queue_size=1)
        rospy.loginfo("DONE INIT CONTROLLER")

    def NavigationChannel(self, red: NnData, green: NnData) -> MotorPowerData:
        gate_mid = 0.5
        if red and green:
            red_bbox_mid = bbox_middle(red)
            green_bbox_mid = bbox_middle(green)
            gate_mid = middle_point(red_bbox_mid, green_bbox_mid)
        elif red:
            red_bbox_mid = bbox_middle(red)
            gate_mid = red_bbox_mid + GATE_MISSING_OFFSET
        elif green:
            green_bbox_mid = bbox_middle(green)
            gate_mid = green_bbox_mid - GATE_MISSING_OFFSET
        else:
            gate_mid = -1.0

        if gate_mid >= 0.45 and gate_mid <= 0.55:
            return motor_power_data_with_refs(NAV_CHANNEL_BASE_SPEED, NAV_CHANNEL_BASE_SPEED, red, green)
        elif gate_mid > 0.65:
            return motor_power_data_with_refs(NAV_CHANNEL_BASE_SPEED - NAV_CHANNEL_TURN_ADJUST, NAV_CHANNEL_BASE_SPEED + NAV_CHANNEL_TURN_ADJUST, red, green)
        elif gate_mid > 0.55:
            return motor_power_data_with_refs(NAV_CHANNEL_BASE_SPEED, NAV_CHANNEL_BASE_SPEED + NAV_CHANNEL_TURN_ADJUST, red, green)
        elif gate_mid < 0.45 and gate_mid != -1.0:
            return motor_power_data_with_refs(NAV_CHANNEL_BASE_SPEED + NAV_CHANNEL_TURN_ADJUST, NAV_CHANNEL_BASE_SPEED - NAV_CHANNEL_TURN_ADJUST, red, green)
        elif gate_mid < 0.35 and gate_mid != -1.0:
            return motor_power_data_with_refs(NAV_CHANNEL_BASE_SPEED + NAV_CHANNEL_TURN_ADJUST, NAV_CHANNEL_BASE_SPEED, red, green)
        else:
            return None

    def MagellansRoute(self, red: NnData, green: NnData) -> MotorPowerData:
        
        return MotorPowerData()
    
    def DockingPlatform(self, course_orientation: bool) -> MotorPowerData:
        return MotorPowerData()
    
    def NorthernPassage(self) -> MotorPowerData:
        return MotorPowerData()

    def send_data(self, evt: rospy.timer.TimerEvent) -> None:
        state = self.state_machine.GetState()
        msg = None

        if state == NAVIGATION_CHANNEL:
            msg = self.NavigationChannel(self.closest_red_buoy, self.closest_green_buoy)
        elif state == MAGELLANS_ROUTE:
            msg = self.MagellansRoute(self.closest_red_buoy, self.closest_green_buoy)
        elif state == DOCKING_PLATFORM:
            msg = self.DockingPlatform(self.closest_red_buoy, self.closest_green_buoy)
        elif state == NORTHERN_PASSAGE:
            msg = self.NorthernPassage(self.closest_red_buoy, self.closest_green_buoy)
        else:
            msg = None

        self._msg_ref_counter += 1
        msg.motor_power_data_ref = self._msg_ref_counter

        if msg:
            self.pub_motor_power.publish(msg)
            rospy.logdebug(f"motors: {msg.left}, {msg.right}")
            rospy.logdebug_throttle(1, f"MSG_SENT: [MotorPowerData: [left={msg.left}, right={msg.right}, motor_power_data_ref={msg.motor_power_data_ref}, nn_data_green_ref={msg.nn_data_green_ref}, nn_data_red_ref={msg.nn_data_red_ref}]]")
        else:
            rospy.logdebug_throttle(1, f"motors: None")
        

if __name__ == "__main__":
    rospy.init_node("core", log_level=rospy.DEBUG)
    ab = AbController()
    rospy.spin()
    