#!/usr/bin/python3

import rospy

from ab23.msg import MotorPowerData
from ab23.msg import NnData

from ab23.srv import CoreTestCtrl, CoreTestCtrlRequest, CoreTestCtrlResponse

import rostest
import unittest

from generators.nn_data_generators import gen_red_green, gen_yellow

import random


class CoreTestBase(unittest.TestCase):

    def core_ctrl_service(self) -> None:
        rospy.logdebug("running tc: {{core_ctrl_service}}")
        rospy.wait_for_service("/core/test/ctrl", None)

        # run asserts for implemented command IDs
        for i in [1, 2, 5]:
            res = self.core_test_ctrl.call(CoreTestCtrlRequest(cmd_id_req=i))
            self.assertEqual(res.cmd_id_res, i)
        
        # run asserts for not implemented IDs TODO(robert): align those ids or find some other way of doing this
        for cmd_id in [3, 4]:
            res = self.core_test_ctrl.call(CoreTestCtrlRequest(cmd_id_req=cmd_id))
            self.assertEqual(res.cmd_id_res, 0)

        # run asserts for not implemented IDs
        for cmd_id in range(6, 256):
            res = self.core_test_ctrl.call(CoreTestCtrlRequest(cmd_id_req=cmd_id))
            self.assertEqual(res.cmd_id_res, 0)


    def core_algorithm_fixed_data(self) -> None:
        rospy.logdebug("running tc: {{core_algorithm_fixed_data}}")
        rospy.wait_for_service("/core/test/ctrl", None)
        res = self.core_test_ctrl.call(CoreTestCtrlRequest(cmd_id_req=2))
        self.assertEqual(res.cmd_id_res, 2)
        rospy.sleep(.1)
        self.pub.publish(NnData(
                bbox_x0=0.6,
                bbox_x1=0.62,
                bbox_y0=0.5,
                bbox_y1=0.7,
                heading_diff=0.0,
                distance=2000.0,
                detection_str=int(4)
            ))
        rospy.sleep(.1)
        self.pub.publish(NnData(
                bbox_x0=0.1,
                bbox_x1=0.12,
                bbox_y0=0.5,
                bbox_y1=0.7,
                heading_diff=0.0,
                distance=2000.0,
                detection_str=int(1)
            ))
        res = self.core_test_ctrl.call(CoreTestCtrlRequest(cmd_id_req=1))
        self.assertEqual(res.cmd_id_res, 1)
        msg = rospy.wait_for_message("/core/motor_power/data", MotorPowerData, None)
        res = self.core_test_ctrl.call(CoreTestCtrlRequest(cmd_id_req=2))
        self.assertEqual(res.cmd_id_res, 2)
        self.assertNotEqual(msg.left, 0)
        self.assertNotEqual(msg.right, 0)
        self.assertLess(msg.left, msg.right)


    def core_algorithm_generated_data(self) -> None:
        rospy.logdebug("running tc: {{core_algorithm_generated_data}}")
        random.seed()
        rospy.wait_for_service("/core/test/ctrl", None)
        res = self.core_test_ctrl.call(CoreTestCtrlRequest(cmd_id_req=2))
        self.assertEqual(res.cmd_id_res, 2)
        # for _ in range(25):
        #     res = self.core_test_ctrl.call(CoreTestCtrlRequest(cmd_id_req=5))
        #     self.assertEqual(res.cmd_id_res, 5)
        #     for _ in range(20):
        #         (red, green) = gen_red_green()
        #         #black = gen_yellow(green, red)
        #         self.pub.publish(green)
        #         self.pub.publish(red)
        #         #self.pub.publish(black)
        self.pub.publish(NnData(
                bbox_x0=0.6,
                bbox_x1=0.62,
                bbox_y0=0.5,
                bbox_y1=0.7,
                heading_diff=0.0,
                distance=2000.0,
                detection_str=int(4)
            ))
        self.pub.publish(NnData(
                bbox_x0=0.1,
                bbox_x1=0.12,
                bbox_y0=0.5,
                bbox_y1=0.7,
                heading_diff=0.0,
                distance=2000.0,
                detection_str=int(1)
            ))
        res = self.core_test_ctrl.call(CoreTestCtrlRequest(cmd_id_req=1))
        self.assertEqual(res.cmd_id_res, 1)
        msg = rospy.wait_for_message("/core/motor_power/data", MotorPowerData, None)
        res = self.core_test_ctrl.call(CoreTestCtrlRequest(cmd_id_req=2))
        self.assertEqual(res.cmd_id_res, 2)
        self.assertNotEqual(msg.left, 0)
        self.assertNotEqual(msg.right, 0)


    def initialize_ros_node(self) -> None:
        rospy.init_node("core_test_suite")    
        if rospy.get_param("/production", True):
            raise Exception("no testing allowed in production environment")
        self.core_test_ctrl = rospy.ServiceProxy("/core/test/ctrl", CoreTestCtrl, True)
        self.pub = rospy.Publisher("/nn/data", NnData, queue_size=2)


    #def test_execute(self) -> None:
    #    self.initialize_ros_node()
    #    self.core_algorithm_fixed_data()
    #    self.core_algorithm_generated_data()
    #    self.core_ctrl_service()



if __name__ == "__main__":
    rostest.rosrun("ab23", "core_test_base", CoreTestBase)

