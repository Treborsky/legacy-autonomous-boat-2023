#!/usr/bin/python3

import rospy


from ab23.srv import Zed2SaveImages, Zed2SaveImagesRequest, Zed2SaveImagesResponse

import rostest
import unittest


class Zed2SrvTestBase(unittest.TestCase):

    def setUp(self) -> None:
        rospy.init_node("zed2_test_suite")    
        if rospy.get_param("/production", True):
            raise Exception("no testing allowed in production environment")
        self.zed2_enable_save_command = rospy.ServiceProxy("/zed2/enable_save_command", Zed2SaveImages, True)


    def test_zed2_service(self) -> None:
        rospy.logdebug("running tests: {{zed2_service}}")
        rospy.wait_for_service("/zed2/enable_save_command", None)

        # run asserts for implemented command IDs
        for i in [1, 2]:
            res = self.zed2_enable_save_command.call(Zed2SaveImagesRequest(requested_cmd=i))
            self.assertEqual(res.executed_cmd, i)

        # run asserts for not implemented IDs
        res = self.zed2_enable_save_command.call(Zed2SaveImagesRequest(requested_cmd=0))
        self.assertEqual(res.executed_cmd, 0)

        for i in range(3, 256):
            res = self.zed2_enable_save_command.call(Zed2SaveImagesRequest(requested_cmd=i))
            self.assertEqual(res.executed_cmd, 0)


if __name__ == "__main__":
    rostest.rosrun("ab23", "zed2_srv_test", Zed2SrvTestBase)
