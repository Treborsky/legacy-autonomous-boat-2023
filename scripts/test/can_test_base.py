#!/usr/bin/python3

import rospy

from ab23.msg import GpsRtkData
from ab23.srv import CanTestCtrl, CanTestCtrlRequest, CanTestCtrlResponse

import rostest
import unittest

import can
import struct

bus = can.interface.Bus(interface='socketcan',channel='can0')

START_PUB = 1
STOP_PUB = 2


#preparing and sending can msg
def send_can_msg() -> None:
    pos = struct.pack('<ii',1,1)

    pos_msg = can.Message(
        arbitration_id=0x134,
        data=pos,
        is_extended_id=False
    )
    
    vel = struct.pack('<hhh',1,1,1)

    vel_msg = can.Message(
        arbitration_id=0x137,
        data=vel,
        is_extended_id=False
    )
    
    hed = struct.pack('<hhhb',1,1,1,1)

    hed_msg = can.Message(
        arbitration_id=0x220,
        data=hed,
        is_extended_id=False
    )
    
    bus.send(pos_msg)
    bus.send(vel_msg)
    bus.send(hed_msg)

class CanTestBase(unittest.TestCase):
    
    def can_ctrl_service(self) -> None:
        rospy.logdebug("running can test: {{can_ctrl_service}}")
        rospy.wait_for_service("/can/test/ctrl", None)
        
        # run asserts for implemented command IDs
        for cmd_id in [1,2]:
            res = self.can_test_ctrl.call(CanTestCtrlRequest(cmd_id_req = cmd_id))
            self.assertEqual(res.cmd_id_res, cmd_id)
    
    #testing sent can msg == produced ros msg       
    def can_sending(self) -> None:
        rospy.logdebug("running can test: {{can_sending_service}}")
        rospy.wait_for_service("/can/test/ctrl", None)
        res = self.can_test_ctrl.call(CanTestCtrlRequest(cmd_id_req = STOP_PUB))
        self.assertEqual(res.cmd_id_res, STOP_PUB)
        rospy.sleep(.1)
        send_can_msg()
        res = self.can_test_ctrl.call(CanTestCtrlRequest(cmd_id_req = START_PUB))
        self.assertEqual(res.cmd_id_res, START_PUB)
        msg = rospy.wait_for_message("/gps_rtk/data", GpsRtkData, None)
        self.assertEqual(msg.latitude,1)
        self.assertEqual(msg.longitude,1)
        self.assertEqual(msg.speed,(1,1,1))
        self.assertEqual(msg.angle_track,1)
        self.assertEqual(msg.angle_slip,1)
        self.assertEqual(msg.curv_rad,1)
        
    
    def initialize_ros_node(self) -> None:
        rospy.init_node("can_test_suite")
        if rospy.get_param("/production", True):
            raise Exception("no testing allowed in production environment")
        self.can_test_ctrl = rospy.ServiceProxy("/can/test/ctrl", CanTestCtrl, True)
        
    def test_execute(self)-> None:
        self.initialize_ros_node()
        self.can_ctrl_service()
        self.can_sending()

if __name__ == "__main__":
    rostest.rosrun("ab23", "can_test_base", CanTestBase)
    