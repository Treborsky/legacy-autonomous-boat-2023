#!/usr/bin/bash
sudo modprobe can
sudo modprobe can_raw
sudo modprobe mttcan
sudo busybox devmem 0x0c303010 32 0x0000C400
sudo busybox devmem 0x0c303018 32 0x0000C458
sudo ip link set can0 type can bitrate 125000 berr-reporting off fd off
sudo ip link set can1 type can bitrate 125000 berr-reporting off fd off
sudo ip link set up can0

