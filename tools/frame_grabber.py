from datetime import datetime
import cv2
import numpy as np
import os

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

parser = ArgumentParser(description="AB23 tool for extracting and saving frames from video for training object detection model.",
                                formatter_class=ArgumentDefaultsHelpFormatter)
parser.add_argument("-vr", "--video-root", dest="video_root", type=str, help="Path for video location", metavar="")
parser.add_argument("-dr", "--data-root", dest="data_root", type=str, help="Path for directory for your output data", metavar="")
parser.add_argument("-dn", "--data-name", dest="data_name", type=str, help="Name for subdirectory e.g. roboboat2023_day1", metavar="")
parser.add_argument("-fps", "--fps", dest="custom_fps", default=5, type=int, help="Saving frames per second (must be below fps for video)", metavar="")
args = vars(parser.parse_args())


def get_saving_frames_durations(cap, saving_fps):
    """A function that returns the list of durations where to save the frames"""
    duration = []
    clip_duration = cap.get(cv2.CAP_PROP_FRAME_COUNT) / cap.get(cv2.CAP_PROP_FPS)
    for i in np.arange(0, clip_duration, 1 / saving_fps):
        duration.append(i)
    print(clip_duration)
    return duration

def main():
    print(video_path)
    data_name_dir = os.path.join(data_path, data_name)
    if not os.path.isdir(data_name_dir):
        os.mkdir(data_name_dir)
    for video in os.listdir(video_path):
        print(video)     
        cap = cv2.VideoCapture(video_path + video)
        fps = cap.get(cv2.CAP_PROP_FPS)
        saving_frames_per_second = min(fps, custom_fps)
        saving_frames_durations = get_saving_frames_durations(cap, saving_frames_per_second)
        frame_cnt = 0 
        while True:
            read, frame = cap.read()
            if not read:
                break
            frame_duration = frame_cnt / fps
            try:
                closest_duration = saving_frames_durations[0]
            except IndexError:
                break
            if frame_duration >= closest_duration:
                cv2.imwrite(os.path.join(data_name_dir, f"frame{frame_cnt}_{datetime.now()}.png"), frame)
                try:
                    saving_frames_durations.pop(0)
                except IndexError:
                    pass
            frame_cnt += 1


if __name__ == "__main__":
    video_path = args["video_root"]
    data_path = args["data_root"]
    data_name = args["data_name"]
    custom_fps = args["custom_fps"]
    main()

