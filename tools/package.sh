#!/bin/bash

cd ${GIT_ROOT}
cd ../.. # currently in workspace root
mv "src/ab23/.git" "src/.git"
tar -zcvf ab23_${CI_COMMIT_SHA}.tar.gz "src/ab23"
mv "src/.git" "src/ab23/.git" # in case we're packaging manually
tar -tf ab23_${CI_COMMIT_SHA}.tar.gz
cd ${GIT_ROOT}
