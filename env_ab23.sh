# AGH Solar Boat - Autonomous Boat 2023 development environment setup script

# load environment and utility variables
export GIT_ROOT=$(git rev-parse --show-toplevel)

# aliases 
## ROS tooling
alias cmi="catkin_make_isolated"
alias cdcmi="cd $GIT_ROOT; cd ../..; catkin_make_isolated"
alias wsclean="cd $GIT_ROOT; cd ../..; rm -rf build_isolated devel_isolated"
alias cleanbuild="wsclean; cdcmi"
alias devsource="cd $GIT_ROOT; source ../../devel_isolated/setup.sh"

## Git aliases
alias gs="git status"
alias ga="git add"
alias gp="git pull"
alias gpp="git pull --prune"
alias gpsh="git push"
alias gck="git checkout"
alias gckb="git checkout -b"
alias gc="git commit"

# VS Code
alias roscode="code $GIT_ROOT"

# Python
export PYTHONPATH="${PYTHONPATH}:${GIT_ROOT}/modules/:${GIT_ROOT}/scripts/"

# Model paths
export MODEL_BEST_PATH="${GIT_ROOT}/model/best.pt"

# TORCH Logging
export FORCE_CUDA=1
